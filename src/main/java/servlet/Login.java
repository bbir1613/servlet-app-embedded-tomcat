package servlet;

import util.Util;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/login")
public class Login extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        PrintWriter out = resp.getWriter();
        if (!username.equals("admin")) {
            req.getSession().setAttribute("login",false);
            out.print(Util.loginHtmlPage);
            out.print("<center><font color=\"red\"> Username incorect</center>");
        }
        else if (!password.equals("admin")) {
            req.getSession().setAttribute("login",false);
            out.print(Util.loginHtmlPage);
            out.print("<center><font color=\"red\"> Password incorect</center>");
        } else {
            req.getSession().setAttribute("login",true);
            req.getRequestDispatcher("/hello").forward(req, resp);
        }
        out.flush();
        out.close();
    }
}
