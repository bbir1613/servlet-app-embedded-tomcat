package util;

public class Util {
    public static String loginHtmlPage = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>Login</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "<div style=\"text-align: center;\">\n" +
            "    <form method = \"POST\" action=\"http://localhost:8080/\">\n" +
            "        <p>Username <input type=\"text\" name=\"username\">\n" +
            "        <p>Password <input type=\"text\" name=\"password\">\n" +
            "        <p><input type=\"submit\" value=\"login\">\n" +
            "    </form>\n" +
            "</div>\n" +
            "</body>\n" +
            "</html>";

    public static String welcomeHtmlPage = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>Welcome</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "<div style=\"text-align: center;\">\n" +
            "    <form method = \"POST\" action=\"http://localhost:8080/\">\n" +
            "        <p>Note <input type=\"text\" name=\"note\">\n" +
            "        <p><input type=\"submit\" value=\"Add\">\n" +
            "    </form>\n" +
            "</div>\n" +
            "</body>\n" +
            "</html>";
}
